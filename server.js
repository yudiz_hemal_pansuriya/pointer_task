const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)

app.use(express.static('public'))

app.get('/', (req, res) => {
    res.sendFile(require('path').join(__dirname, './public/index.html'))
})

io.on('connection', (socket) => {
    console.log('connected id: ', socket.id);

    socket.on('message', (x, y) => {
        socket.broadcast.emit('received', x,y)
    })

    socket.on('disconnect', () => {
        console.log('disconnected id: ', socket.id)
    })
})

server.listen(8080, () => {
    console.log('listening on 8080')
})